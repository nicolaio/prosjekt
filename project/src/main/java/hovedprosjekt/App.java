package hovedprosjekt;


import javafx.application.Application;

import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.stage.Stage;



// TODO: Auto-generated Javadoc
/**
 * The Class App.
 */
public class App extends Application{
	
	/** The prev player score. */
	private int prevPlayerScore;
	
	/** The prev bot score. */
	private int prevBotScore;
	
	/** The name. */
	private String name;

	
	/**
	 * Start.
	 *
	 * @param primaryStage the primary stage
	 * @throws Exception the exception
	 */
	@Override
	public void start(final Stage primaryStage) throws Exception{
		
	    FXMLLoader loader = new FXMLLoader(App.class.getResource("FXML-meny.fxml"));
        Scene sc  = new Scene(loader.load());
        //Må lagre instansen av controlleren vår for å kunne endre score og navn i den i den 
        MenyController controller = loader.<MenyController>getController();
        
	    
	    try {
	        //Dette kan ikke skje før vinduet faktisk er opprettet som tar litt tid, derfor må den teste dette først (Nullptr feil ellers)
	        controller.setScore(prevPlayerScore, prevBotScore);
	     }
	    catch(NullPointerException ex) {
	    	System.err.println("NullPointerException: " + ex.getMessage());
	    }

	    
		primaryStage.setTitle("Spillet");
		primaryStage.setScene(sc);
		primaryStage.show();
	}
	

	/**
	 * The main method.
	 *
	 * @param args the arguments
	 */
	public static void main(String[] args) {
		App.launch(args);
	}
	
	
	/**
	 * Sets the score.
	 *
	 * @param playerScore the player score
	 * @param botScore the bot score
	 */
	public void setScore(int playerScore, int botScore){
		this.prevPlayerScore = playerScore;
		this.prevBotScore = botScore;
	}
	
	/**
	 * Sets the name for an App instance.
	 *
	 * @param the new name
	 */
	public void setName(String name) {
		this.name = name;
	}
	
	/**
	 * Gets the player score.
	 *
	 * @return the player score
	 */
	public int getPlayerScore() {
		return prevPlayerScore;
	}
	
	/**
	 * Gets the bot score.
	 *
	 * @return the bot score
	 */
	public int getBotScore() {
		return prevBotScore;
	}
	


}
