package hovedprosjekt;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeMap;



public class SortedLeaderboard extends Leaderboard{

	public SortedLeaderboard(String filename) {
		super(filename);
		this.hashMap = docuMapping();	
		sortingLeaderboard();
	}
	
	/**
	 * Sorting the leaderboard in list form.
	 */
	
	
	public void sortingLeaderboard() {
		//this.hashMap = docuMapping();
		LinkedHashMap<String, Integer> diffMap = new LinkedHashMap<String, Integer>();
		for(String key : hashMap.keySet()) {
			ArrayList<Integer> intList = hashMap.get(key);
			int difference = intList.get(0).intValue() - intList.get(1).intValue();
			diffMap.put(key, difference);
		}
		Set<Map.Entry<String, Integer>> entrySet = 
                diffMap.entrySet();
	
		List<Map.Entry<String, Integer>> ListEntry = 
            new ArrayList<Map.Entry<String, Integer>>(entrySet);
	
		
		 Collections.sort(ListEntry, 
	                new Comparator<Map.Entry<String, Integer>>() {
	 
	            @Override
	            public int compare(Entry<String, Integer> es1, 
	                    Entry<String, Integer> es2) {
	                return es2.getValue().compareTo(es1.getValue());
	            }
	        });

		 
		 LinkedHashMap<String, ArrayList<Integer>> prevMap = new LinkedHashMap<String, ArrayList<Integer>>(hashMap);
		 this.hashMap.clear();
	 
		 for(Map.Entry<String, Integer> map : ListEntry){
	            this.hashMap.put(map.getKey(), prevMap.get(map.getKey()));
	        }
	}
}
