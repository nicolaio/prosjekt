package hovedprosjekt;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Scanner;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;

import javafx.scene.control.TextField;
import javafx.stage.Stage;


// TODO: Auto-generated Javadoc
/**
 * The Class MenyController.
 */
public class MenyController {
	
	/** The saved name. */
	String saveName;
	private static String FILENAME = "scores.txt";
	
	/** The leaderboard */
	Leaderboard liste = new Leaderboard(FILENAME);
	
	/** The leaderboard in hashmap form. */
	public LinkedHashMap<String, ArrayList<Integer>> leaderboard = liste.hashMap;
	
	/** The previous score. */
	private int prevScore = 0;	   
	
	/** The bot score. */
	private int botScore = 0;
	
	
	/** The textfield navn. */
	@FXML 
	public TextField navn;
	
	

	/** Difficulity easy */
	@FXML
	private Button vgLett;
	
	/** Difficulity medium. */
	@FXML
	private Button vgMiddels;
	
	/** Difficulity hard. */
	@FXML
	private Button vgVanskelig;
	
	/**
	 * If easy.
	 *
	 * @param This happens when "lett" buttom is pushed
	 */
	@FXML

	public void onLett() {
		if(saveName == null) {
			Alert.display("Du må skrive inn navn for å spille eller endre om det allerede er i bruk!");
			return;
		}
		Stage stage = (Stage) vgLett.getScene().getWindow();
	    stage.close();
	
		Stage gameStage = new Stage();
		Spill game = new Spill();

		//set difficulity 
		game.setDifficulity(1);
		game.setName(navn.getText());

		

		try {
			game.start(gameStage);
		} catch (Exception e) {
			e.getCause().getClass().equals(AssertionError.class);
			e.printStackTrace();
		}
		
		
	}
	
	/**
	 * On medium.
	 */
	@FXML
	public void onMiddels() {
		if(saveName == null) {
			Alert.display("Du må skrive inn navn for å spille eller endre om det allerede er i bruk!");
			return;
		}

		Stage stage = (Stage) vgMiddels.getScene().getWindow();
	    stage.close();
		
		Stage gameStage = new Stage();
		Spill game = new Spill();
		game.setDifficulity(2);
		game.setName(navn.getText());
		try {
			game.start(gameStage);
		} catch (Exception e) {
			e.getCause().getClass().equals(AssertionError.class);
			e.printStackTrace();
		}
		
		
	}
		
	
	/**
	 * On hard.
	 */
	@FXML
	public void onVanskelig() {
		if(saveName == null) {
			Alert.display("Du må skrive inn navn for å spille eller endre om det allerede er i bruk!");
			return;
		}
		
		Stage stage = (Stage) vgVanskelig.getScene().getWindow();
	    stage.close();
		
		Stage gameStage = new Stage();
		Spill game = new Spill();
		game.setDifficulity(3);	
		game.setName(navn.getText());
		try {
			game.start(gameStage);
		} catch (Exception e) {
			e.getCause().getClass().equals(AssertionError.class);
			e.printStackTrace();
		}
		
		
	}

	
	
	/**
	 * On leaderboard button.
	 */
	@FXML
	public void onLeaderBoard() {
	try {
		
		 FXMLLoader loader = new FXMLLoader(getClass().getResource("Leaderboard.fxml"));
	     Scene sc  = new Scene(loader.load());
	     LeaderboardController lcontroller = loader.getController();
	     lcontroller.textOutput();
	     Stage stage = new Stage();
	     stage.setScene(sc);
	     lcontroller.stage = stage;
	     stage.show();
	     
		
	} catch (IOException e) {
		e.printStackTrace();
	}
			
	}
	
	
	
	/**
	 * On lagre navn button.
	 */
	@FXML
	public void onLagreNavn(){
		if(liste.duplicate_name_check(navn.getText())) {
			Alert.display("Dette navnet er allerede i bruk! Finn et annet");
			return;
		}
		saveName = navn.getText();
	}
		
	
	/**
	 * Sets the scores.
	 *
	 * @param sets the player score 
	 * @param sets the bot score
	 */
	public void setScore(int score, int botScore){
		this.prevScore = score;
		this.botScore = botScore;
	}
	
	/**
	 * Sets the name.
	 *
	 * @param name the new name
	 */
	public void setName(String name) {
		this.saveName = name;
	}
	
	/**
	 * Gets the player score.
	 *
	 * @return the player score
	 */
	public int getPlayerScore() {
		return prevScore;
	}
	
	/**
	 * Gets the bot score.
	 *
	 * @return the bot score
	 */
	public int getBotScore() {
		return botScore;
	}
	
	
}
