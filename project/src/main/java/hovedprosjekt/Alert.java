package hovedprosjekt;

import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;

// TODO: Auto-generated Javadoc
/**
 * The Class Alert.
 */
public class Alert {
	
	/**
	 * Display.
	 *
	 * @param message shown in the alert
	 */
	public static void display(String message) {
		Stage stage = new Stage();
		stage.initModality(Modality.APPLICATION_MODAL);
		stage.setTitle("Advarsel");
		stage.setMinWidth(400);
		
		Label l = new Label();
		l.setText(message);
		Button close_button = new Button("Okei!");
		close_button.setOnAction(e -> stage.close());
		
		VBox layout = new VBox(15);
		layout.getChildren().addAll(l, close_button);
		layout.setAlignment(Pos.CENTER);
		
		Scene scene = new Scene(layout);
		stage.setScene(scene);
		stage.showAndWait();
	}
}
