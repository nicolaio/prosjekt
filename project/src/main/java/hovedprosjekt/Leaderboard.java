package hovedprosjekt;

import java.io.File;
import javafx.scene.control.Label;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Scanner;
import java.util.TreeMap;

// TODO: Auto-generated Javadoc
/**
 * The Class SortLeaderboard. A bit 
 */
public class Leaderboard {
	
	/** The Constant FILENAME. */


	protected String FILENAME;
	protected LinkedHashMap<String, ArrayList<Integer>> hashMap;
	

	/**
	 * Instantiates a new leaderboard.
	 */
	public Leaderboard(String filename){
		this.FILENAME = filename;
		hashMap = docuMapping();
	}
	
	/**
	 * Document mapping, create a linkedHashMap out of the textfile to be able to sort it.
	 *
	 * @return the linked hash map
	 */
	protected LinkedHashMap<String, ArrayList<Integer>> docuMapping() {
		
		LinkedHashMap<String, ArrayList<Integer>> hashMapping = new LinkedHashMap<String, ArrayList<Integer>>();
		try {
			File file = new File(FILENAME);
			Scanner myReader = new Scanner(file);

			while (myReader.hasNextLine()) {
				String line = myReader.nextLine(); 							//Leser neste linje
				String[] splittedLine = line.split("\t"); 					//splitter linja p� tabulator
				String name = splittedLine[0]; 								//navnet er det f�rste elementet
				int playerScore = Integer.parseInt(splittedLine[1]); 		// spillerscore er 2. element
				int botScore = Integer.parseInt(splittedLine[2]); 			// botScore er 3.element
				ArrayList<Integer> intList = new ArrayList<Integer>();		//lager liste med disse elementene
				intList.add((Integer)playerScore);							//
				intList.add((Integer)botScore);								//legger disse scorene i en liste som Integers
				hashMapping.put(name, intList);								// legger navn og integerlista inn i hashmappet
		    }
			myReader.close();
		} catch (FileNotFoundException e) {
			throw new IllegalArgumentException("invalid file");
		} 
		return hashMapping;
	}
		
	
	
	public LinkedHashMap<String, ArrayList<Integer>> getHashmap(){
		return hashMap;
	}
	public void setHashMap(LinkedHashMap<String, ArrayList<Integer>> map) {
		this.hashMap = map;
	}
	
	
	/**
	 * Over write score to file.
	 */
	public void overWriteScore() {
		File file = new File(FILENAME);
		try {
			FileWriter fw = new FileWriter(file, false);
			
			for (String key : hashMap.keySet()) {
				ArrayList<Integer> scores = new ArrayList<Integer>();
				scores = hashMap.get(key);
				Iterator<Integer> iterator = scores.iterator();
				fw.write(key + "\t" + iterator.next() + "\t" + iterator.next() + "\n");
			}
			fw.close();
			
			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	

	public boolean duplicate_name_check(String name) {
		if(hashMap.keySet().contains(name)) {
			return true;
		}
		return false;
	}
	public String getFileName() {
		return FILENAME;
	}
	
}
