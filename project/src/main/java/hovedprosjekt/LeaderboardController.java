package hovedprosjekt;

import java.io.IOException;

import java.util.ArrayList;
import java.util.LinkedHashMap;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.stage.Stage;


// TODO: Auto-generated Javadoc
/**
 * The Class LeaderboardController.
 */
public class LeaderboardController {
	
	private static String FILENAME = "scores.txt";
	
	/** The player name. */
	@FXML public Label playerName;
	
	/** The player score. */
	@FXML public Label playerScore;
	
	/** The bot score. */
	@FXML public Label botScore;
	
	/** The sort button. */
	@FXML private Button sortButton;
	
	/** This stage. */
	public Stage stage = null;
	
	/**
	 * On sort.
	 *
	 * @param event the event
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	@FXML
	public void onSort(ActionEvent event) throws IOException {
		SortedLeaderboard sort = new SortedLeaderboard(FILENAME);
		sort.overWriteScore();
		textOutput();
		
	}

	
	/**
	 * On close.
	 *
	 * @param event the event
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	@FXML
	public void onClose(ActionEvent event) throws IOException{
		stage.close();
	}

	/**
	 * Text output.
	 */
	public void textOutput() {
		StringBuilder nameString = new StringBuilder();
		StringBuilder pscoreString = new StringBuilder();
		StringBuilder bscoreString = new StringBuilder();
		Leaderboard lb = new Leaderboard("scores.txt");
		LinkedHashMap<String, ArrayList<Integer>> hashMap = lb.getHashmap();
		for (String key : hashMap.keySet()) {
			nameString.append(key);
			nameString.append("\n");
			
			Integer a = hashMap.get(key).get(0);
			Integer b = hashMap.get(key).get(1);
			
			pscoreString.append(a);
			pscoreString.append("\n");
			bscoreString.append(b);
			bscoreString.append("\n");
			
		}
		playerName.setText(nameString.toString());
		playerScore.setText(pscoreString.toString());
		botScore.setText(bscoreString.toString());
		
	}
	
	/**
	 * On close.
	 *
	 * @param event the event
	 * @throws IOException Signals that an I/O exception has occurred.
	 */

}
