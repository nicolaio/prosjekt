package hovedprosjekt;

import java.io.FileWriter;
import java.io.IOException;
import java.util.Random;
import javafx.scene.input.KeyEvent;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.TextAlignment;
import javafx.stage.Stage;
import javafx.util.Duration;


// TODO: Auto-generated Javadoc
/**
 * The Class Spill. This is our JavaFX game of Pong.
 */
public class Spill extends Application{
	
	/** The height. */
	private static int height = 600;
	
	/** The width. */
	private static int width = 1000;
	
	/** The player heigth. */
	private static int player_heigth = 100;
	
	/** The player width. */
	private static int player_width  = 15;
	
	/** The ball radius. */
	private static double ball_radius = 15;
	
	/** True if game started. */
	private boolean game_started = false;
	
	/** True if game ended. */
	private boolean game_ended = false;
	
	/** The player one pos x direction. */
	private double player_one_pos_x = 0;
	
	/** The player one pos y direction. */
	private double player_one_pos_y = height / 2;
	
	/** The player two pos x direction. */
	private double player_two_pos_x = width-player_width;
	
	/** The player two pos y direction. */
	private double player_two_pos_y = height / 2;
	
	/** The ball pos x direction. */
	public double ball_pos_x = width/2;
	
	/** The ball pos y direction. */
	private double ball_pos_y = height/2;
	
	/** The difficulity. */
	private double difficulity = 1;
	
	/** The ball speed x  direction. */
	private double ball_speed_x = 1;
	
	/** The ball speed y direction. */
	private double ball_speed_y = 1;
	
	/** The score p 1. */
	private int score_p1 = 0;
	
	/** The score p 2. */
	private int score_p2 = 0;
	
	/** The Name. */
	private String Name = "yolo";
	

	
	/** This stage. */
	private Stage thisStage = null;
	
	/** This timeline. */
	private Timeline thisTimeline = null;
	
	
	/**
	 * Start. What happens when the application starts.
	 *
	 * @param theStage our stage
	 * @throws Exception throws if start casts exception
	 */
	//Lager banen
	public void start(Stage theStage) throws Exception
    {
    	Canvas canvas = new Canvas(width, height);
		GraphicsContext gc = canvas.getGraphicsContext2D();
		Timeline tl = new Timeline(new KeyFrame(Duration.millis(10), e -> run(gc)));
		tl.setCycleCount(Timeline.INDEFINITE);
		thisStage = theStage;
		thisTimeline = tl;
		
		
		player_movement(theStage);
		
		theStage.addEventHandler(KeyEvent.KEY_PRESSED, (key) -> {
		      if(key.getCode()==KeyCode.ENTER) {
		    	  game_started = true;
		      }
		});
		theStage.addEventHandler(KeyEvent.KEY_PRESSED, (key) -> {
		      if(key.getCode()==KeyCode.Q) {
		    	  game_ended = true;
		      }
		});
		ball_speed_x = difficulity;
    	ball_speed_y = difficulity;
		
		theStage.setScene(new Scene(new StackPane(canvas)));
		theStage.setTitle("spelllla");
        theStage.show();
        tl.play();
    }
	
	
	/**
	 * Start game.
	 *
	 * @param gameStarted, true if the game is supposed to start
	 * @param gc, our GraphicalContext
	 */
	//starten på ett nytt spill
	void startGame(boolean gameStarted, GraphicsContext gc) {
		
		//Lager banen
		gc.setStroke(Color.YELLOW);
		gc.setTextAlign(TextAlignment.CENTER);
		
		gc.setFill(Color.BLACK);
		gc.fillRect(0, 0, width, height);
		gc.setFill(Color.WHITE);
		gc.setFont(Font.font(25));
		
		if(!gameStarted) {
			gc.strokeText("Press enter to start, Q to quit", width / 2, height / 2);
			gc.strokeText("Good luck " + Name, width / 2, height / 2 + 40);
			}
	}
	
	
	/**
	 * Run. This is whats running when the game is running.
	 *
	 * @param gc, our GraphicalContext
	 */
	//setter retning på farten tilfeldig og med fart litt vanskelighetsgrad
	 private void run(GraphicsContext gc) {
	    	if(game_ended) {game_ended(thisStage);}
	    	startGame(game_started, gc);
	    	gc.fillRect(player_one_pos_x, player_one_pos_y, player_width, player_heigth);
			gc.fillRect(player_two_pos_x, player_two_pos_y, player_width, player_heigth);
			gc.fillText(score_p1 + "\t\t\t\t\t\t\t\t" + score_p2, width / 2, 100);
			if(game_started) {
				game_logic(gc);}
			else {
				ball_speed_x = new Random().nextInt(2) == 0 ? difficulity: -difficulity;
				ball_speed_y = new Random().nextInt(2) == 0 ? difficulity: -difficulity;
			}
	    }

	
	/**
	 * Player movement. Le
	 *
	 * @param theStage the the stage
	 */
	public void player_movement(Stage theStage) {
		theStage.addEventHandler(KeyEvent.KEY_PRESSED, (key) -> {
		      if(key.getCode()==KeyCode.UP) {
		    	  if(!(player_one_pos_y==0)) {
		          player_one_pos_y-=15;}
		      }
		});
		
		theStage.addEventHandler(KeyEvent.KEY_PRESSED, (key) -> {
		      if(key.getCode()==KeyCode.DOWN) {
		    	  if(!(player_one_pos_y==(height-player_heigth))) {
		          player_one_pos_y+=15;}
		      }
		});
		
	}

	/**
	 * Game logic. This what controls our game.
	 *
	 * @param gc, our GraphicalContext
	 */
	public void game_logic(GraphicsContext gc) {
		
		ball_pos_x+=ball_speed_x;
		ball_pos_y+=ball_speed_y;
		
		gc.fillOval(ball_pos_x, ball_pos_y, ball_radius, ball_radius);
		//Edges up and down
		if(ball_pos_y > height || ball_pos_y < 0) {ball_speed_y *=-1;}
		
		//points
		if(ball_pos_x < player_one_pos_x - player_width) {
			score_p2++;
			game_started = false;
			player_one_pos_y = height/2;
			player_two_pos_y = height/2;
			ball_pos_x = width/2;
			ball_pos_y = height/2;
		}
		if(ball_pos_x > player_two_pos_x + player_width) {  
			score_p1++;
			game_started = false;
			player_one_pos_y = height/2;
			player_two_pos_y = height/2;
			ball_pos_x = width/2;
			ball_pos_y = height/2;
		}
		
		//turn right
		if((ball_pos_x + ball_radius > player_two_pos_x) && (ball_pos_y >= player_two_pos_y) && (ball_pos_y <= player_two_pos_y + player_heigth)) {
			ball_speed_y *= new Random().nextInt(2) == 0 ? 1: -1;
			ball_speed_x *= -1.2;
		}
		//turn left
		if((ball_pos_x < player_width + player_one_pos_x) && (ball_pos_y >= player_one_pos_y) && (ball_pos_y <= player_one_pos_y + player_heigth)) {
			ball_speed_y *= new Random().nextInt(2) == 0 ? 1: -1;
			ball_speed_x *= -1.2;
		}
		//autopilot up
		if((ball_pos_y < player_two_pos_y+50)&&(ball_pos_x>width/2)&&(player_two_pos_y>0)) {
			player_two_pos_y-=difficulity*Math.abs((ball_pos_y - player_two_pos_y + 50))/70;
			
		}
		//autopilot down
		if((ball_pos_y > player_two_pos_y+50)&&(ball_pos_x>width/2)&&((player_two_pos_y + player_heigth + difficulity*(ball_pos_y - player_two_pos_y + 50)/70) < height)) {
			player_two_pos_y+=difficulity*Math.abs(ball_pos_y - player_two_pos_y + 50)/70;
			
		}
		
		
	}
	
	/**
	 * Game ended. If gameEnded variable is true, our game close and you come back to the men
	 * 
	 * . The method closes the game stage and stops the timeline.
	 *
	 * @param theStage, our stage
	 */
	
	public void game_ended(Stage theStage){
		theStage.close();
		thisTimeline.stop();
		Stage menyStage = new Stage();
		App meny = new App();
		meny.setScore(score_p1, score_p2);
		meny.setName(Name);
		try {
			meny.start(menyStage);
		} catch (Exception e) {
			e.getCause().getClass().equals(AssertionError.class);
			e.printStackTrace();
		}
		
		try
		{
		    String filename= "scores.txt";
		    FileWriter fw = new FileWriter(filename, true); //the true will append the new data
		    fw.write(Name + "\t" + score_p1 + "\t" + score_p2 + "\n");//appends the string to the file
		    fw.close();
		    
		}catch(IOException ioe){
		    System.err.println("IOException: " + ioe.getMessage());
		}
		

				
	}
 
        
   
	/**
	 * Gets the difficulity.
	 *
	 * @return the difficulity
	 */
	public double getDifficulity() {
		return difficulity;
	}

	/**
	 * Sets the difficulity.
	 *
	 * @param difficulity the new difficulity
	 */
	public void setDifficulity(double difficulity) {
		this.difficulity = difficulity;
	}
    
	/**
	 * Sets the name.
	 *
	 * @param name the new name
	 */
	public void setName(String name) {
		this.Name = name;
	}
	public Stage getStage() {
		return thisStage;
	}
	public Timeline getTimeline() {
		return thisTimeline;
	}
   
}
