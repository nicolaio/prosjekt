import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.FileNotFoundException;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertThrows;

import hovedprosjekt.Leaderboard;
import hovedprosjekt.Leaderboard;

public class LeaderboardTest {
	private Leaderboard slb;
	private static String FILENAME = "scores.txt";
	
	@BeforeEach
	public void testInvalidFileName() {
		assertThrows(FileNotFoundException.class, () -> {Leaderboard slb = new Leaderboard("ikkescores.txt");});
	}
	
	@Test
	void testConstructor() {
		
	}
}
