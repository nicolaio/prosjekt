package tests;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.ArrayList;
import java.util.LinkedHashMap;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import hovedprosjekt.Leaderboard;
import hovedprosjekt.SortedLeaderboard;

public class SortedLeaderboardTest {
	private SortedLeaderboard slb;
	private LinkedHashMap<String, ArrayList<Integer>> hashMap;
	
	@BeforeEach
	public void setup() {
		slb = new SortedLeaderboard("scores.txt");
	}
	
	@Test	
	public void constructorTest() {
		assertEquals(slb.getFileName(), "scores.txt");
	}
	
	@Test
	public void test_sorting() {
		Leaderboard lb = new Leaderboard("scores.txt");
		assertEquals(slb.getHashmap(), lb.getHashmap());
	}
		
}
