package tests;

import org.junit.jupiter.api.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Scanner;
import java.util.Set;

import hovedprosjekt.Leaderboard;
import hovedprosjekt.SortedLeaderboard;

public class LeaderboardTest {
	private static String FILENAME = "scores.txt";

	@Test
	public void testConstructor() {
		Leaderboard lb = new Leaderboard(FILENAME);
	}
	
	
	@Test
	public void testInvalidFileName() {
		assertThrows(IllegalArgumentException.class, () -> {Leaderboard slb = new Leaderboard("ikkescores.txt");});
	}
	
	@Test
	public void testOverWriteScore() {
		File file = new File(FILENAME);
		try {
			Scanner scanner1 = new Scanner(file);
			SortedLeaderboard heyo = new SortedLeaderboard(FILENAME);
			heyo.overWriteScore(); //overskriver s� filene ikke lenger er like
			Scanner scanner2 = new Scanner(file);
			while (scanner1.hasNextLine() || scanner2.hasNextLine()) {
				assertFalse(scanner1 == scanner2);
			}
		} catch (FileNotFoundException e) {
			throw new IllegalArgumentException("invalid file");
		} 
	}
	
	
	
}
